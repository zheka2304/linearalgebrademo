import re
import copy

from parsers import *
from .number import *
from .fraction import *
from modules import *


def _cast_to_fraction(factor):
    if isinstance(factor, Fraction):
        return Fraction(fraction=factor)
    if isinstance(factor, Number):
        return Fraction(numerator=factor)
    elif isinstance(factor, int):
        return Fraction(numerator=Number(factor))


class Polynomial(Parsable):
    def __init__(self, factors=None):
        if factors is None:
            factors = []

        self.factors = []
        for factor in factors:
            self.factors.append(_cast_to_fraction(factor))
        self.validate()

    def parse(self, parser, span):
        span = span.span(2, len(span) - 2)
        factors = re.split("[^A-Za-z0-9/\\-]", str(span))

        self.factors = []
        for factor in factors:
            if factor:
                factor = _cast_to_fraction(parser.parse(ParserSpan(factor)))
                if factor is None:
                    return False
                self.factors.append(factor)
        return True

    def validate(self):
        if len(self.factors) == 0:
            self.factors = [Fraction(numerator=Number(0))]
        while len(self.factors) > 1 and self.factors[-1].is_zero():
            self.factors.pop()
        return True

    def __copy__(self):
        return Polynomial(factors=copy.copy(self.factors))

    def __str__(self):
        s = ""
        i = 0
        for factor in self.factors:
            if not factor.is_zero() or i == 0:
                s += (" + " if len(s) > 0 else "") + str(factor) + ("*x^" + str(i) if i > 0 else "")
            i += 1
        return "[[" + s + "]]"

    def __len__(self):
        return len(self.factors)

    def __getitem__(self, key):
        if key >= len(self.factors):
            return Fraction(numerator=Number(0))
        return self.factors[key]

    def __setitem__(self, key, val):
        val = _cast_to_fraction(val)
        if val is None:
            raise TypeError()
        while len(self.factors) <= key:
            self.factors.append(Fraction(numerator=Number(0)))
        self.factors[key] = val

    def __iter__(self):
        return self.factors.__iter__()

    def is_zero(self):
        for factor in self.factors:
            if not factor.is_zero():
                return False
        return True

    def opposite(self):
        factors = []
        for factor in self.factors:
            factors.append(factor.opposite())
        return Polynomial(factors)

    def __eq__(self, other):
        if isinstance(other, Polynomial):
            return bool(call_module("com_pp_eq", self, other))
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return self == Polynomial(factors=[_cast_to_fraction(other)])
        else:
            return False

    def __ne__(self, other):
        return not self == other

    def __add__(self, other):
        if isinstance(other, Polynomial):
            return call_module("add_pp_p", self, other)
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("add_pp_p", self, Polynomial(factors=[_cast_to_fraction(other)]))
        else:
            raise TypeError()

    def __sub__(self, other):
        if isinstance(other, Polynomial):
            return call_module("sub_pp_p", self, other)
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("sub_pp_p", self, Polynomial(factors=[_cast_to_fraction(other)]))
        else:
            raise TypeError()

    def __mul__(self, other):
        if isinstance(other, Polynomial):
            return call_module("mul_pp_p", self, other)
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("mul_pq_p", self, _cast_to_fraction(other))
        else:
            raise TypeError()

    def __floordiv__(self, other):
        if isinstance(other, Polynomial):
            return call_module("div_pp_p", self, other)
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("div_pp_p", self, Polynomial(factors=[_cast_to_fraction(other)]))
        else:
            raise TypeError()

    def __mod__(self, other):
        if isinstance(other, Polynomial):
            return call_module("mod_pp_p", self, other)
        elif isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("mod_pp_p", self, Polynomial(factors=[_cast_to_fraction(other)]))
        else:
            raise TypeError()

    def __radd__(self, other):
        if isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("add_pp_p", Polynomial(factors=[_cast_to_fraction(other)]), self)
        else:
            raise TypeError()

    def __rsub__(self, other):
        if isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("sub_pp_p", Polynomial(factors=[_cast_to_fraction(other)]), self)
        else:
            raise TypeError()

    def __rmul__(self, other):
        if isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("mul_pp_p", Polynomial(factors=[_cast_to_fraction(other)]), self)
        else:
            raise TypeError()

    def __rfloordiv__(self, other):
        if isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("div_pp_p", Polynomial(factors=[_cast_to_fraction(other)]), self)
        else:
            raise TypeError()

    def __rmod__(self, other):
        if isinstance(other, Number) or isinstance(other, int) or isinstance(other, Fraction):
            return call_module("mod_pp_p", Polynomial(factors=[_cast_to_fraction(other)]), self)
        else:
            raise TypeError()

    def __neg__(self):
        return self.opposite()

    def __abs__(self):
        factors = []
        for factor in self.factors:
            factors.append(abs(factor))
        return Polynomial(factors)


# Register
add_value_parser("^\[\[([^A-Za-z0-9_\/\[\]]*[-+]?[A-Za-z0-9_]{1,}(\/[-+]?[A-Za-z0-9_]{1,}){0,1}[^A-Za-z0-9_\/\[\]]*)*\]\]", Polynomial, priority=0)
__all__ = ["Polynomial"]
