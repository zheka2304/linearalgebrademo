from parsers import *


class Boolean(Parsable):
    def __init__(self, source=False):
        self.value = False
        if isinstance(source, Boolean):
            self.value = source.value
        else:
            self.value = bool(source)

    def parse(self, parser, span):
        s = str(span).lower()
        if s == "true":
            self.value = True
        elif s == "false":
            self.value = False
        else:
            return False
        return True

    def validate(self):
        return True

    def __bool__(self):
        return self.value

    def __str__(self):
        return str(self.value)

    def __copy__(self):
        return Boolean(self)

    def __eq__(self, other):
        if not isinstance(other, Boolean) and not isinstance(other, bool):
            return False
        return bool(self) == bool(other)

    def __ne__(self, other):
        return not (self == other)


add_value_parser("^(\w+)", Boolean, priority=-1)  # Lowest priority
