import copy

from modules import *
from parsers import *
from .number import *


class Fraction(Parsable):
    def __init__(self, numerator=None, denominator=None, negative=None, fraction=None, reduction=True):
        if fraction is not None:
            numerator = fraction.numerator
            denominator = fraction.denominator
            negative = fraction.negative
            reduction = False
        if denominator is None:
            denominator = 1
            reduction = False
        numerator = Number(numerator)
        denominator = Number(denominator)

        self.numerator = numerator
        self.denominator = denominator
        if negative is None:
            self._calc_sign()
        else:
            self.negative = negative
        if reduction:
            self.reduction()
        self._validate_sign()

    def _calc_sign(self):
        self.negative = bool(self.numerator.negative) != bool(self.denominator.negative)
        self.numerator.negative = False
        self.denominator.negative = False
        self._validate_sign()

    def parse(self, parser, span):
        s = str(span)
        sp = s.split("/")
        if len(sp) != 2:
            sp = s.split(".")
            if len(sp) != 2:
                return False
            integer_str = sp[0].strip()
            floating_str = sp[1].strip()
            numerator = parser.parse(ParserSpan(integer_str + floating_str))
            if len(floating_str) != 0:
                denominator = Number()
                denominator[len(floating_str)] = 1
            else:
                denominator = Number(1)
        else:
            numerator = parser.parse(ParserSpan(sp[0].strip()))
            denominator = parser.parse(ParserSpan(sp[1].strip()))

        if isinstance(numerator, Number) and isinstance(denominator, Number):
            self.numerator = numerator
            self.denominator = denominator
            self._calc_sign()
            self.reduction()
            return True
        else:
            return False

    def _validate_sign(self):
        if self.numerator.negative and self.denominator.negative:
            self.numerator.negative = False
            self.denominator.negative = False

    def validate(self):
        self._validate_sign()
        return self.numerator.validate() and self.denominator.validate() and not self.denominator.is_zero()

    def __copy__(self):
        return Fraction(fraction=self, reduction=False)

    def __str__(self):
        if self.numerator.is_zero():
            return "0"
        return ("-" if self.negative else "") + str(self.numerator) + "/" + str(self.denominator)

    def is_zero(self):
        return self.numerator.is_zero()

    def opposite(self):
        return Fraction(numerator=self.numerator, denominator=self.denominator, negative=not self.negative, reduction=False)

    def reduction(self, force=False):
        if (force or get_instance().get_option("reduction")) and not self.is_zero() and not self.denominator.is_zero():
            f = call_module("red_q_q", self)
            self.numerator = f.numerator
            self.denominator = f.denominator
            self.negative = f.negative
            self._validate_sign()

    def __int__(self):
        return int(call_module("int", self))

    def __lt__(self, other):
        if isinstance(other, Fraction):
            return bool(call_module("com_qq_lt", self, other))
        elif isinstance(other, int) or isinstance(other, Number):
            return self < Fraction(numerator=Number(other))
        else:
            raise TypeError()

    def __eq__(self, other):
        if isinstance(other, Fraction):
            return bool(call_module("com_qq_eq", self, other))
        elif isinstance(other, int) or isinstance(other, Number):
            return self == Fraction(numerator=Number(other))
        else:
            return False

    def __le__(self, other):
        return self < other or self == other

    def __ne__(self, other):
        return not self == other

    def __gt__(self, other):
        if isinstance(other, Fraction):
            return other < self
        if isinstance(other, int) or isinstance(other, Number):
            return Fraction(numerator=Number(other)) < self
        else:
            raise TypeError()

    def __ge__(self, other):
        if isinstance(other, Fraction):
            return other <= self
        if isinstance(other, int) or isinstance(other, Number):
            return Fraction(numerator=Number(other)) <= self
        else:
            raise TypeError()

    def __add__(self, other):
        if isinstance(other, Fraction):
            return call_module("add_qq_q", self, other)
        elif isinstance(other, Number) or isinstance(other, int):
            return call_module("add_qq_q", self, Fraction(numerator=Number(other)))
        else:
            raise TypeError()

    def __sub__(self, other):
        if isinstance(other, Fraction):
            return call_module("sub_qq_q", self, other)
        elif isinstance(other, Number) or isinstance(other, int):
            return call_module("sub_qq_q", self, Fraction(numerator=Number(other)))
        else:
            raise TypeError()

    def __mul__(self, other):
        if isinstance(other, Fraction):
            return call_module("mul_qq_q", self, other)
        elif isinstance(other, Number) or isinstance(other, int):
            return call_module("mul_qq_q", self, Fraction(numerator=Number(other)))
        else:
            raise TypeError()

    def __truediv__(self, other):
        if isinstance(other, Fraction):
            return call_module("div_qq_q", self, other)
        elif isinstance(other, Number) or isinstance(other, int):
            return call_module("div_qq_q", self, Fraction(numerator=Number(other)))
        else:
            raise TypeError()

    def __pow__(self, other):
        if isinstance(other, Number) or isinstance(other, int):
            other = Number(other)
            result = Fraction(numerator=1)
            i = Number(0)
            while i < other:
                result *= self
                i += 1
            return result
        elif isinstance(other, Fraction) and other.denominator == 1:
            return self ** call_module("trans_q_z", other)
        else:
            raise TypeError()

    def __radd__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return self + Fraction(numerator=other)
        else:
            raise TypeError()

    def __rsub__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return Fraction(numerator=other) - self
        else:
            raise TypeError()

    def __rmul__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return self * Fraction(numerator=other)
        else:
            raise TypeError()

    def __rtruediv__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return Fraction(numerator=other)/self
        else:
            raise TypeError()

    def __rpow__(self, other):
        if isinstance(other, int) or isinstance(other, Number):
            return Fraction(numerator=other) ** self
        else:
            raise TypeError()

    def __neg__(self):
        return self.opposite()

    def __abs__(self):
        if self.negative:
            return self.opposite()
        else:
            return self


# Register
add_value_parser("^[-+]?[0-9]+\/[0-9]+", Fraction, priority=2)
add_value_parser("^[-+]?[0-9]+\.[0-9]*", Fraction, priority=2)
add_value_parser("^[-+]?[0-9]*\.[0-9]+", Fraction, priority=2)
__all__ = ["Fraction"]
