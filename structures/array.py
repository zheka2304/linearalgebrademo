from parsers import *


class Array(Parsable):
    def __init__(self, src=None):
        if src is None:
            src = []
        self.value = src[:]

    # Arrays or arrays are too complex to be parsed from string with current system
    def parse(self, parser, span):
        s = ParserSpan(str(span)[2:-1])
        self.value = []
        while len(s) > 0:
            val = parser.parse(s)
            if val is not None:
                self.value.append(val)
            elif s[0].isspace() or s[0] in [',']:
                s += 1
            else:
                return False
        return True

    def validate(self):
        for val in self.value:
            if not val.validate():
                return False
        return True

    def append(self, val):
        self.value.append(val)
        self.check_and_raise()

    def insert(self, i, val):
        self.value.insert(i, val)
        self.check_and_raise()

    def pop(self, i):
        return self.value.pop(i)

    def __copy__(self):
        return self

    def __str__(self):
        vals = []
        for val in self.value:
            if type(val).__name__ == "String":
                vals.append('"' + str(val) + '"')
            else:
                vals.append(str(val))
        return "LIST[" + ", ".join(vals) + "]"

    def __len__(self):
        return len(self.value)

    def __eq__(self, other):
        if not isinstance(other, Array):
            return False
        if len(self) != len(other):
            return False
        for i in range(len(self)):
            if self.value[i] != other.value[i]:
                return False
        return True

    def __neg__(self, other):
        return not (self == other)

    def __ne__(self, other):
        return not (self == other)

    def check_recursion(self, hashes=None):
        if hashes is None:
            hashes = []
        if self in hashes:
            return False
        hashes.append(self)
        for val in self.value:
            if isinstance(val, Array) and not val.check_recursion(hashes=hashes):
                return False
        return True

    def check_and_raise(self):
        if not self.check_recursion():
            self.value = []
            raise ParserException("array has self references, it will be destroyed")

    def __getitem__(self, item):
        return self.value[item]

    def __setitem__(self, key, val):
        self.value[key] = val
        self.check_and_raise()


add_value_parser("^L\[([^\[\]\{\}\\\"\\\']|\[\[[^\[\]\{\}\\\"\\\']*\]\]|\\\"[^\\\"]*\\\"|\\\'[^\\\']*\\\')*\]", Array)
__all__ = ["Array"]
