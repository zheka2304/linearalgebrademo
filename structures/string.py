from parsers import *


class String(Parsable):
    def __init__(self, src=""):
        self.value = str(src)

    def parse(self, parser, span):
        self.value = str(span)[1:-1]
        return True

    def validate(self):
        return True

    def __copy__(self):
        return String(self.value)

    def __str__(self):
        return self.value

    def __len__(self):
        return len(self.value)

    def __eq__(self, other):
        if not isinstance(other, String) and not isinstance(other, str):
            return False
        return str(self) == str(other)

    def __ne__(self, other):
        return not (self == other)

    def __getitem__(self, item):
        return self.value[item]

    def __setitem__(self, key, val):
        self.value = self.value[:key] + val + self.value[key + 1:]


# Register
add_value_parser("^\\\"[^\\\"]*\\\"", String)
add_value_parser("^\\\'[^\\\']*\\\'", String)
