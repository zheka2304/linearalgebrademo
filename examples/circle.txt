# CIRCLE
# Just draws a circle, using built-in trigonometry modules


set_global_flag("reduction", False);

window := graph_create("trig", "600x600");

PI2 := 2. * 355/113;
STEP := PI2 / 90.;

x0 := 300/1; y0 := 300/1; rad := 250/1;
lastX := x0 - rad;
lastY := y0;
for (i := 0.; i <= PI2; i := i + STEP) {
	x := x0 - cosf(i) * rad;
	y := y0 - sinf(i) * rad;
	graph_line(window, lastX, lastY, x, y, "green");
	lastX := x;
	lastY := y;
}
