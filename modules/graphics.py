# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *
from interface.window import get_root_window

import tkinter as tk
import uuid


_graphics_windows = {}


class GraphicsWindow(tk.Toplevel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.uid = str(uuid.uuid4())
        _graphics_windows[self.uid] = self

        self.canvas = tk.Canvas(self, bg="white")
        self.canvas.pack(fill=tk.BOTH, expand=1)
        root = get_root_window()
        if root is not None:
            root.add_child_win(self)


def graph_create(name, size=None):
    win = GraphicsWindow()
    win.title(name.value)
    if size is not None:
        win.geometry(str(size))
    return String(win.uid)


def graph_destroy(_handle):
    handle = str(_handle)
    if handle in _graphics_windows:
        _graphics_windows[handle].destroy()
        del _graphics_windows[handle]
    return _handle


def graph_clear(_handle):
    handle = str(_handle)
    if handle in _graphics_windows:
        _graphics_windows[handle].canvas.delete("all")
    return _handle


def graph_erase(_handle, tag):
    handle = str(_handle)
    if handle in _graphics_windows:
        _graphics_windows[handle].canvas.delete(tag.value)
    return _handle


def graph_line(_handle, x1, y1, x2, y2, color):
    handle = str(_handle)
    if handle in _graphics_windows:
        canvas = _graphics_windows[handle].canvas
        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2)
        y2 = int(y2)
        tag = str(uuid.uuid4())
        canvas.create_line(x1, y1, x2, y2, fill=color.value, tag=tag)
        return String(tag)
    return String("")


def graph_text(_handle, x, y, text, color, size):
    handle = str(_handle)
    if handle in _graphics_windows:
        canvas = _graphics_windows[handle].canvas
        x = int(x)
        y = int(y)
        tag = str(uuid.uuid4())
        canvas.create_text(x, y, text=text.value, fill=color.value, font=(None, int(size)), tag=tag)
        return String(tag)
    return String("")
        

register_module("graph_create", graph_create, String, (String, ), "opens window for graphics with given name, returns handle")
register_module("graph_create", graph_create, String, (String, String), "opens window for graphics with given name and geometry, returns handle")
register_module("graph_destroy", graph_destroy, String, (String, ), "destroys window by handle")
register_module("graph_clear", graph_clear, String, (String, ), "clears window by handle")
register_module("graph_erase", graph_erase, String, (String, String), "erases element by its tag")
register_module("graph_line", graph_line, String, (String, Fraction, Fraction, Fraction, Fraction, String), "draws a line in given a window, returns element tag")
register_module("graph_line", graph_line, String, (String, Number, Number, Number, Number, String), "draws line in given a window, returns element tag")
register_module("graph_text", graph_text, String, (String, Fraction, Fraction, String, String, Number), "draws text in given a window, returns element tag")
register_module("graph_text", graph_text, String, (String, Number, Number, String, String, Number), "draws text in given a window, returns element tag")

__all__ = []


