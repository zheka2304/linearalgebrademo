# Created by Dmitry Elizarov  09.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *

def mul_nk_n(a, k):
    if k == 0:
        return a
    m = int(k)
    b = NNumber(0)
    b[m-1] = 0
    i = 0
    w = len(a) - 1
    while i <= w:
        b[m] = a[i]
        m += 1
        i += 1
    return b


register_module("mul_nk_n",
                mul_nk_n, NNumber, (NNumber, NNumber),
                """
                Multiplies natural number by 10 ^ k
                """)
__all__ = []

