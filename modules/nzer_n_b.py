# Created by Dmitry Elizarov  08.03.2019
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def nzer_n_b(a):
    if a == 0:
        return Boolean(False)
    if a != 0:
        return Boolean(True)


register_module("nzer_n_b",
                nzer_n_b, Boolean, (NNumber, ),
                """
                Checks if given number is zero
                """)

__all__ = []


