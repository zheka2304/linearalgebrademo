# Made by Pavel Klyosov
# 13.03.2019 04:30

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def trans_z_q(a):
    return Fraction(numerator=abs(a), denominator=1,negative=a.negative)


register_module("trans_z_q",
                trans_z_q, Fraction, (Number,),
                """
                Converts integer to fraction
                """)

__all__ = []

