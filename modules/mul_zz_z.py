# Made by Pavel Klyosov
# 12.03.2019 20:54

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mul_zz_z(a, b):
    if (a >= 0 and b >= 0) or (a <= 0 and b <= 0):
        return call_module("mul_nn_n", NNumber(abs(a)), NNumber(abs(b)))
    else:
        return -call_module("mul_nn_n", NNumber(abs(a)), NNumber(abs(b)))


register_module("mul_zz_z",
                mul_zz_z, Number, (Number, Number),
                """
                Multiplies two integers
                """)

__all__ = []

