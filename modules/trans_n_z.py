# Created by Helen 11.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def trans_n_z(a):
    b = Number(a)
    return b


register_module("trans_n_z", trans_n_z, Number, (NNumber, ), "Convert natural to integer")

__all__ = []

