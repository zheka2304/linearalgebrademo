# Created by Pavel Klesov 08.03.2019

# Additional modules for logic implementation
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def logic_bb_and(a, b):
    return Boolean(a and b)


def logic_bb_or(a, b):
    return Boolean(a or b)


def logic_bb_xor(a, b):
    return Boolean(bool(a) != bool(b))


def logic_bb_eq(a, b):
    return Boolean(bool(a) == bool(b))


def logic_bb_impl(a, b):
    return Boolean(not a or b)


def logic_bb_not(a):
    return Boolean(not a)


register_module("logic_bb_and", logic_bb_and, Boolean, (Boolean, Boolean), "Logic and")
register_module("logic_bb_or", logic_bb_or, Boolean, (Boolean, Boolean), "Logic or")
register_module("logic_bb_xor", logic_bb_xor, Boolean, (Boolean, Boolean), "Logic xor")
register_module("logic_bb_eq", logic_bb_eq, Boolean, (Boolean, Boolean), "Logic equality")
register_module("logic_bb_impl", logic_bb_impl, Boolean, (Boolean, Boolean), "Logic implication")
register_module("not", logic_bb_not, Boolean, (Boolean, ), "Logic not operator")

__all__ = []
