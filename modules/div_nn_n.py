# Created by Helen 10.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mod_div_nn_n(a, b):
    result = NNumber()
    while a >= b:
        c = call_module("div_nn_dk", a, b)
        d = call_module("div_nn_k", a, b)
        a = call_module("sub_ndn_n", a, d, c)
        result[len(d) - len(b)] = c[0]
    return result, a


def div_nn_n(a, b):
    if b.is_zero():
        return ErrorResult("cannot divide by zero: " + str(a) + " / " + str(b))
    if a < b:
        return NNumber(0)
    return mod_div_nn_n(a, b)[0]


def mod_nn_n(a, b):
    if b.is_zero():
        return ErrorResult("cannot divide by zero: " + str(a) + " / " + str(b))
    if a < b:
        return a
    return mod_div_nn_n(a, b)[1]


register_module("div_nn_n", div_nn_n, NNumber, (NNumber, NNumber, ), "Divides two naturals numbers, returns quotient")

register_module("mod_nn_n", mod_nn_n, NNumber, (NNumber, NNumber, ), "Divides two naturals numbers, returns remainder")

__all__ = []
