# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def cast_z_q(x):
    return Fraction(numerator=x)


def cast_zz_q(x, y):
    return Fraction(numerator=x, denominator=y)


def cast_q_z(x):
    result = x.numerator // x.denominator
    result.negative = x.negative
    return Number(result)


def cast_q_q_red(x):
    x.reduction(force=True)
    return x


def numerator(f):
    n = Number(f.numerator)
    n.negative = f.negative
    return n


def denominator(f):
    return f.denominator


register_module("frac",
                cast_z_q, Fraction, (Number, ),
                """ Casts integer to fraction """)

register_module("frac",
                cast_zz_q, Fraction, (Number, Number),
                """ Creates fraction from numerator and denominator integers """)

register_module("frac",
                cast_q_q_red, Fraction, (Fraction, ),
                """ Force fraction reduction, even if it is disabled """)

register_module("int",
                cast_q_z, Number, (Fraction, ),
                """ Casts fraction to integer (value is floored) """)

register_module("numerator",
                numerator, Number, (Fraction, ),
                """ Returns fraction numerator """)

register_module("denominator",
                denominator, Number, (Fraction, ),
                """ Returns fraction denominator """)

__all__ = []
