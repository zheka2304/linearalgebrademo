# Created by Dmitry Elizarov 10.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *


def trans_q_z(a):
    if a.denominator != 1:
        raise TypeError()
    else:
        result = Number(a.numerator)
        result.negative = a.negative
        return result


register_module("trans_q_z",
                trans_q_z, Number, (Fraction, ),
                """
                Fraction to integer conversion (if the denominator is 1)
                """)

__all__ = []
