# Made py Pavel Klyosov
# 21.03.2019


# import module registry
from .registry import *

# import structures

from structures import *


def mod_pp_p(a, b):
    result = Polynomial([])
    result = a - call_module("div_pp_p", a, b) * b
    return result


register_module("mod_pp_p", mod_pp_p, Polynomial, (Polynomial, Polynomial, ),
                "Divides two polynomials, returns remainder")

__all__ = []


