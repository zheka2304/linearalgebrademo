# Created by Pavel Klesov 08.03.2019

# Additional modules to make operator support for fractions
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


# a == b
def com_qq_eq(a, b):
    if a.is_zero() and b.is_zero():
        return Boolean(True)
    elif a.is_zero() or b.is_zero():
        return Boolean(False)
    a.reduction(force=True)
    b.reduction(force=True)
    return Boolean(
            a.numerator == b.numerator and
            a.denominator == b.denominator and
            a.negative == b.negative
            )


register_module("com_qq_eq", com_qq_eq, Boolean, (Fraction, Fraction), "Returns if two fractions are equal")


# a != b
def com_qq_neq(a, b):
    return Boolean(not call_module("com_qq_eq", a, b))


register_module("com_qq_neq", com_qq_neq, Boolean, (Fraction, Fraction), "Returns if two fractions are not equal")


# a < b
def com_qq_lt(a, b):
    if a.is_zero() and b.is_zero():
        return Boolean(False)
    elif a.is_zero():
        return Boolean(not b.negative)
    elif b.is_zero():
        return Boolean(a.negative)
    elif not a.negative and b.negative:
        return Boolean(False)
    elif a.negative and not b.negative:
        return Boolean(True)
    elif a.negative and b.negative:
        return call_module("com_qq_lt", b.opposite(), a.opposite())
    elif not a.negative and not b.negative:
        a.reduction(force=True)
        b.reduction(force=True)
        left = a.numerator * b.denominator
        right = a.denominator * b.numerator
        return Boolean(left < right)


register_module("com_qq_lt", com_qq_lt, Boolean, (Fraction, Fraction), "Returns if first fraction is lesser than second")


# a > b
def com_qq_gt(a, b):
    return call_module("com_qq_lt", b, a)


register_module("com_qq_gt", com_qq_gt, Boolean, (Fraction, Fraction), "Returns if second fraction is lesser than first")


# a <= b
def com_qq_le(a, b):
    return Boolean(call_module("com_qq_lt", a, b) or call_module("com_qq_eq", a, b))


register_module("com_qq_le", com_qq_le, Boolean, (Fraction, Fraction), "Returns if first fraction is lesser or equal to second")


# a >= b
def com_qq_ge(a, b):
    return Boolean(call_module("com_qq_gt", a, b) or call_module("com_qq_eq", a, b))


register_module("com_qq_ge", com_qq_ge, Boolean, (Fraction, Fraction), "Returns if second fraction is lesser or equal to first")

__all__ = []
