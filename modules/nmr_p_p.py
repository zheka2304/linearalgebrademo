# Made py Pavel Klyosov
# 21.03.2019


# import module registry
from .registry import *

# import structures
from structures import *


def nmr_p_p(a):
    der_a = Polynomial([])
    gcf_a = Polynomial([])
    result = Polynomial([])
    der_a = call_module("der_p_p", a)
    gcf_a = call_module("gcf_pp_p", a, der_a)
    result = call_module("div_pp_p", a, gcf_a)
    return result


register_module("nmr_p_p", nmr_p_p, Polynomial, (Polynomial, ),
                "Returns changed polynomial")

__all__ = []

