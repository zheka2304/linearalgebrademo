# Made by Pavel Klyosov
# 13.03.2019 02:10

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def gcf_pp_p(a, b):
    while a != 0 and b != 0:
        a = a % b
        temp = a
        a = b
        b = temp
    return a if a != 0 else b


register_module("gcf_pp_p",
                gcf_pp_p, Polynomial, (Polynomial, Polynomial),
                """
                Return GCF of two polynomials
                """)

__all__ = []

