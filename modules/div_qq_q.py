# Made by Pavel Klyosov
# 13.03.2019 05:50

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def div_qq_q(a, b):
    result = Fraction(numerator=abs(a.numerator * b.denominator), denominator=abs(a.denominator * b.numerator))
    if (a >= 0 and b >= 0) or (a <= 0 and b <= 0):
        result.negative = False
    else:
        result.negative = True
    return result


register_module("div_qq_q",
                div_qq_q, Fraction, (Fraction, Fraction,),
                """
                Divides two fractions
                """)

__all__ = []

