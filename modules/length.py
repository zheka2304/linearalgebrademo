# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def length(x):
    return NNumber(len(x))


register_module("len", length, NNumber, (Number, ), """ Returns number digit count """)
register_module("len", length, NNumber, (Polynomial, ), """ Returns polynomial degree + 1 """)
register_module("len", length, NNumber, (String, ), """ Returns length of string """)
register_module("len", length, NNumber, (Array, ), """ Returns length of array """)

__all__ = []
