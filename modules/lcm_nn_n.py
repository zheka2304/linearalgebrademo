# Made by Dmitry Elizarov
# 13.03.2019 05:30

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def lcm_nn_n(a, b):
    return a * b // call_module("gcf_nn_n", a, b)


register_module("lcm_nn_n",
                lcm_nn_n, NNumber, (NNumber, NNumber, ),
                """
                Returns LCM of two numbers
                """)
__all__ = []

