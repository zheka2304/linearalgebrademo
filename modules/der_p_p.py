# Created by Dmitry Elizarov 14.03.2019
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def der_p_p(a):
    i = 1
    w = len(a)
    a[0] = Fraction(numerator=0)
    while i <= w:
        a[i] = i * a[i]
        a[i-1] = a[i]
        i += 1
    return a


register_module("der_p_p",
                der_p_p, Polynomial, (Polynomial, ),
                """
                Returns polynomial derivative
                """)

__all__ = []
