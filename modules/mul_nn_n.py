# Created by Helen 10.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mull_nn_n(a, b):
    result = NNumber()
    for i in range(len(b)):
        c = call_module("mul_nd_n", a, NNumber(b[i]))
        c = call_module("mul_nk_n", c, NNumber(i))
        result = call_module("add_nn_n", result, c)
    return result


register_module("mul_nn_n", mull_nn_n, NNumber, (NNumber, NNumber, ), "Multiplies two natural numbers")

__all__ = []

