# Created by Helen 6.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def sub_nn_n(a, b):
    if b > a:
        return ErrorResult("subtracting greater from lesser natural number: " + str(a) + " - " + str(b))

    for i in range(max(len(a), len(b))):
        a[i] -= b[i]
        if a[i] < 0:
            a[i] += 10
            a[i + 1] -= 1
    return a


register_module("sub_nn_n", sub_nn_n, NNumber, (NNumber, NNumber, ), "Subtracts two natural numbers")

__all__ = []
