# Created by Helen 10.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mul_nd_n(a, b):
    result = NNumber()
    if b > 9:
        return ErrorResult("passed non-digit to mul_nd_n")
    for i in range(len(a)):
        result[i] += a[i] * b[0]
        result[i + 1] = result[i] // 10
        result[i] %= 10
    return result


register_module("mul_nd_n", mul_nd_n, NNumber, (NNumber, NNumber, ), "Multiplies natural number by digit")

__all__ = []
