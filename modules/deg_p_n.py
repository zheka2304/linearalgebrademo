# Created by Dmitry Elizarov 14.03.2019
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def deg_p_n(a):
    a = len(a) - 1
    return Number(a)


register_module("deg_p_n",
                deg_p_n, Number, (Polynomial, ),
                """
                Returns degree of polynomial
                """)

__all__ = []
