# Created by Dmitry Elizarov 13.03.2019
# import module registry
from .registry import *
# import structures like Number, Fraction, ...
from structures import *


def add_qq_q(a, b):
    lcm = Number(call_module("lcm_nn_n", NNumber(a.denominator), NNumber(b.denominator)))
    sum_a = a.numerator * (lcm // a.denominator)
    sum_b = b.numerator * (lcm // b.denominator)
    top = (sum_a.opposite() if a.negative else sum_a) + (sum_b.opposite() if b.negative else sum_b)
    return Fraction(numerator=top, denominator=lcm)


register_module("add_qq_q",
                add_qq_q, Fraction, (Fraction, Fraction),
                """
                Returns sum of two fractions
                """)
__all__ = []

