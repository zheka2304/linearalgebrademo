# Made by Pavel Klyosov
# 21.03.2019

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mul_pp_p(a, b):
    result = Polynomial([])
    for i in range(len(b)):
        c = call_module("mul_pxk_p", a, NNumber(i))
        c = call_module("mul_pq_p", c, b[i])
        result = result + c
    return result


register_module("mul_pp_p", mul_pp_p, Polynomial, (Polynomial, Polynomial, ),
                "Multiplies two polynomials")

__all__ = []
