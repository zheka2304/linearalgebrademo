# Created by Dmitry Elizarov and Zubareva Elena 07.02.2019
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def poz_z_d(a):
    if a.is_zero():
        return NNumber(0)
    if a.negative:
        return NNumber(1)
    else:
        return NNumber(2)


register_module("poz_z_d",
                poz_z_d, NNumber, (Number, ),
                "Checks sign of number, returns 1 if number is negative, 2 if positive and 0 if its zero")

__all__ = []
