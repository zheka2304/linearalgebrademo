# Made by Dmitry Elizarov
# 13.03.2019 04:30

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def add_zz_z(a, b):
    if a.negative == b.negative:
        result = Number(call_module("add_nn_n", NNumber(abs(a)), NNumber(abs(b))))
        result.negative = a.negative
    else:
        if abs(a) >= abs(b):
            result = Number(call_module("sub_nn_n", NNumber(abs(a)), NNumber(abs(b))))
            result.negative = a.negative
        else:
            result = Number(call_module("sub_nn_n", NNumber(abs(b)), NNumber(abs(a))))
            result.negative = b.negative
    return result


register_module("add_zz_z",
                add_zz_z, Number, (Number, Number,),
                """
                Returns sum of two integers
                """)
__all__ = []

