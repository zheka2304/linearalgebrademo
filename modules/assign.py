# Created by Pavel Klesov 09.03.2019

# Module to implement variable assignment
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from parsers import *


def assign(var, val):
    var.assign(val)
    return val


register_module("__assign_variable", assign, Parsable, (Variable, Parsable), "implementation of := operator, cannot be called")

__all__ = []
