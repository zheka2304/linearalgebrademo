# Created by Helen 10.03.19(C)
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def div_nn_dk(a, b):
    i = 0
    if a < b:
        return ErrorResult("Second parameter greater then first")
    c = call_module("mul_nk_n", b, NNumber(len(a) - len(b)))
    if a < c:
        c = call_module("mul_nk_n", b, NNumber(len(a) - len(b) - 1))
    while call_module("mul_nn_n", c, NNumber(i)) <= a:
        i += 1
    return NNumber(i - 1), c


def div_nn_d(a, b):
    return div_nn_dk(a, b)[0]


def div_nn_k(a, b):
    return div_nn_dk(a, b)[1]


register_module("div_nn_dk", div_nn_d, NNumber, (NNumber, NNumber, ),
                "Calculating the first digits dividing a greater number of natural into smaller, multiplied by 10 ^ k")

register_module("div_nn_k", div_nn_k, NNumber, (NNumber, NNumber, ),
                "Support module, returns number c from previous module")

__all__ = []

