# Made by Pavel Klyosov
# 12.03.2019 23:20

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mod_zz_z(a, b):
    return a - (a // b) * b


register_module("mod_zz_z",
                mod_zz_z, Number, (Number, Number),
                """
                Divides two integers, returns remainder
                """)

__all__ = []
