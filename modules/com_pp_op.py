# Created by Pavel Klesov 13.03.2019

# Additional modules to make operator support for fractions
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


# a == b
def com_pp_eq(a, b):
    i = max(len(a), len(b)) - 1
    while i >= 0:
        if a[i] != b[i]:
            return Boolean(False)
        i -= 1
    return Boolean(True)


register_module("com_pp_eq", com_pp_eq, Boolean, (Polynomial, Polynomial), "returns if two polynomials are equal")


# a != b
def com_pp_neq(a, b):
    return Boolean(not call_module("com_pp_eq", a, b))


register_module("com_pp_neq", com_pp_neq, Boolean, (Polynomial, Polynomial), "returns if two polynomials are not equal")

__all__ = []
