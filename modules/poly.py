# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def poly_i(val, degree):
    p = Polynomial()
    p[int(degree)] = Fraction(numerator=val)
    return p


def poly_f(val, degree):
    p = Polynomial()
    p[int(degree)] = val
    return p


def factor(p, degree):
    return p[int(degree)]


def substitute(p, x):
    f = Fraction(numerator=1)
    result = Fraction(numerator=0)
    for i in range(len(p)):
        result = result + p[i] * f
        f = x * f
    return result


register_module("poly",
                poly_i, Polynomial, (Number, NNumber),
                """ Converts integer to polynomial of given degree """)

register_module("poly",
                poly_f, Polynomial, (Fraction, NNumber),
                """ Converts fraction to polynomial of given degree """)

register_module("factor",
                factor, Fraction, (Polynomial, NNumber),
                """ Returns factor for degree """)

register_module("substitute",
                substitute, Fraction, (Polynomial, Fraction),
                """ Substitutes given value into a polynomial """)

__all__ = []
