# Made by Pavel Klyosov
# 21.03.2019

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def fac_p_q(a):
    m = a[0].denominator
    for i in range(len(a)):
        if a[i] != Fraction(numerator=0):
            m = call_module("lcm_nn_n", NNumber(m), NNumber(a[i].denominator))
    d = a[0].numerator
    for i in range(len(a)):
        if a[i] != Fraction(numerator=0):
            d = call_module("gcf_nn_n", NNumber(d), NNumber(a[i].numerator))

    return Fraction(numerator=d, denominator=m)


register_module("fac_p_q", fac_p_q, Fraction, (Polynomial, ),
                "Returns polynomials numerators LCM and denominators GCF")

__all__ = []


