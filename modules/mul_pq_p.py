# Made by Pavel Klyosov
# 14.03.2019 02:00

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mul_pq_p(a, b):
    i = len(a) - 1
    while i >= 0:
        a[i] = a[i] * b
        i -= 1
    return a


def mul_qp_p(a, b):
    return mul_pq_p(b, a)


register_module("mul_pq_p",
                mul_pq_p, Polynomial, (Polynomial, Fraction, ),
                """
                Multiplies polynomial by fraction
                """)

register_module("mul_qp_p",
                mul_qp_p, Polynomial, (Fraction, Polynomial, ),
                """
                Multiplies polynomial by fraction
                """)

__all__ = []

