import re

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def str_format(s, x):
    try:
        args = [x]
        for i in range(len(re.findall("\{[^\{\}]*\}", s.value)) - 1):
            args.append("{}")
        return String(s.value.format(*tuple(args)))
    except IndexError as e:
        return s


def str_add(s1, s2):
    return String(s1.value + s2.value)


register_module("str_format", str_format, String, (String, Parsable, ), """ Formats one value in the string, formation flag is \"{}\" """)
register_module("str_add", str_add, String, (String, String, ), """ Adds two strings """)

__all__ = []
