# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def com_nn_d(a, b):
    i = max(len(a), len(b)) - 1
    while i >= 0:
        if a[i] > b[i]:
            return NNumber(2)
        if a[i] < b[i]:
            return NNumber(1)
        i -= 1
    return NNumber(0)


register_module("com_nn_d",
                com_nn_d, NNumber, (NNumber, NNumber),
                """
                Compares two integers, returns 2 if first is larger, 1 if second is larger and 0 if they are equal
                """)

__all__ = []

