import re

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def get_global_flag_b(name):
    return Boolean(get_instance().get_option(name.value))


def set_global_flag_b(name, val):
    get_instance().set_option(name.value, bool(val))
    return val


register_module("get_global_flag_b", get_global_flag_b, Boolean, (String, ), """ Gets global boolean flag """)
register_module("set_global_flag", set_global_flag_b, Boolean, (String, Boolean, ), """ Sets global boolean flag """)

__all__ = []
