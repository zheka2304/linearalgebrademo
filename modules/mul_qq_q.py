# Made by Pavel Klyosov
# 13.03.2019 05:40

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def mul_qq_q(a, b):
    result = Fraction(numerator=abs(a.numerator * b.numerator), denominator=abs(a.denominator * b.denominator))
    result.negative = a.negative != b.negative
    return result


register_module("mul_qq_q",
                mul_qq_q, Fraction, (Fraction, Fraction,),
                """
                Multiplies two fractions
                """)

__all__ = []

