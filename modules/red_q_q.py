# Made by Pavel Klyosov
# 13.03.2019 03:20

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def red_q_q(a):
    gcf = Number(call_module("gcf_nn_n", NNumber(a.numerator), NNumber(a.denominator)))
    a.numerator = a.numerator // gcf
    a.denominator = a.denominator // gcf
    return a


register_module("red_q_q",
                red_q_q, Fraction, (Fraction,),
                """
                Returns an abbreviated fraction, this module will be automatically call for input fractions
                """)

__all__ = []
