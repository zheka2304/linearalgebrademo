# Created by Pavel Klesov 08.03.2019

# Additional modules for logic implementation
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def index_number_get(n, i):
    return Number(n[int(i)])


def index_number_set(n, i, val):
    if val > 9 or val < 0:
        return ErrorResult("setting invalid digit to number " + str(val))
    n[int(i)] = int(val)
    return n


def index_poly_get(p, i):
    return p[int(i)]


def index_poly_set(p, i, val):
    p[int(i)] = val
    return p


def index_str_get(s, i):
    return String(s[int(i)])


def index_str_set(s, i, val):
    s[int(i)] = str(val)
    return s


def index_arr_get(a, i):
    try:
        return a[int(i)]
    except IndexError as e:
        return ErrorResult(str(e))


def index_arr_set(a, i, val):
    try:
        a[int(i)] = val
        return a
    except IndexError as e:
        return ErrorResult(str(e))


def arr_append(a, val):
    a.append(val)
    return a


def arr_insert(a, index, val):
    try:
        a.insert(int(index), val)
        return a
    except IndexError as e:
        return ErrorResult(str(e))


def arr_delete(a, index):
    try:
        return a.pop(int(index))
    except IndexError as e:
        return ErrorResult(str(e))


def arr_copy(a):
    return Array(src=a.value)


def arr_concat(a1, a2):
    arr = Array(a1.value + a2.value)
    arr.check_and_raise()
    return arr


def com_arr_eq(a1, a2):
    return Boolean(a1 == a2)


def com_arr_neq(a1, a2):
    return Boolean(a1 != a2)


register_module("index", index_number_get, Number, (Number, Number), "Returns number digit by index")
register_module("index", index_number_set, Number, (Number, Number, Number), "Sets number digit by index to given value, returns modified number")
register_module("index", index_poly_get, Fraction, (Polynomial, Number), "Returns polynomial factor for given degree")
register_module("index", index_poly_set, Polynomial, (Polynomial, Number, Fraction), "Sets polynomial factor for given degree, returns modified polynomial")
register_module("index", index_str_set, String, (String, Number), "Returns character from string by index")
register_module("index", index_str_set, String, (String, Number, String), "Inserts string to replace character at given index, returns modified string")
register_module("index", index_arr_get, Array, (Array, Number), "Returns value of array element for given index")
register_module("index", index_arr_set, Array, (Array, Number, Parsable), "Sets value to array element for given index, take into account that array is an object and all modifications will apply to first parameter")
register_module("append", arr_append, Array, (Array, Parsable), "Appends item to end of array, take into account that array is an object and all modifications will apply to first parameter")
register_module("insert", arr_insert, Array, (Array, NNumber, Parsable), "Inserts item into array to given index, take into account that array is an object and all modifications will apply to first parameter")
register_module("remove", arr_delete, Parsable, (Array, NNumber), "Removes and returns item from array at given index, take into account that array is an object and all modifications will apply to first parameter")
register_module("copy", arr_copy, Array, (Array, ), "Returns copy of array")
register_module("arr_concat", arr_concat, Array, (Array, Array), "Returns sum of arrays")
register_module("com_arr_eq", com_arr_eq, Array, (Array, Array), "Returns if arrays are equal")
register_module("com_arr_neq", com_arr_neq, Array, (Array, Array), "Returns if arrays are not equal")

__all__ = []

