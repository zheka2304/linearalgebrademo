# Made by Pavel Klyosov
# 14.03.2019 01:20

# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def sub_pp_p(a, b):
    result = Polynomial([0])
    i = max(len(a), len(b)) - 1
    while i >= 0:
        result[i] = a[i] - b[i]
        i -= 1
    return result


register_module("sub_pp_p",
                sub_pp_p, Polynomial, (Polynomial, Polynomial, ),
                """
                Subtracts two polynomials
                """)

__all__ = []

