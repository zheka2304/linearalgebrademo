# Created by Pavel Klesov 08.03.2019

# Additional modules to make operator support for integers
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


# a == b
def com_zz_eq(a, b):
    if a.negative == b.negative or a.is_zero():
        return Boolean(str(call_module("com_nn_d", NNumber(a), NNumber(b))) == "0")
    else:
        return Boolean(False)


register_module("com_zz_eq", com_zz_eq, Boolean, (Number, Number), "Returns if two integers are equal")


# a != b
def com_zz_neq(a, b):
    return Boolean(not call_module("com_zz_eq", a, b))


register_module("com_zz_neq", com_zz_neq, Boolean, (Number, Number), "Returns if two integers are not equal")


# a < b
def com_zz_lt(a, b):
    if a.is_zero() and b.is_zero():
        return Boolean(False)
    elif a.is_zero():
        return Boolean(not b.negative)
    elif b.is_zero():
        return Boolean(a.negative)
    elif not a.negative and b.negative:
        return Boolean(False)
    elif a.negative and not b.negative:
        return Boolean(True)
    elif a.negative and b.negative:
        return Boolean(str(call_module("com_nn_d", NNumber(a), NNumber(b))) == "2")
    elif not a.negative and not b.negative:
        return Boolean(str(call_module("com_nn_d", NNumber(a), NNumber(b))) == "1")
    else:
        return ErrorResult("Assertion error")


register_module("com_zz_lt", com_zz_lt, Boolean, (Number, Number), "Returns if first integer is lesser than second")


# a > b
def com_zz_gt(a, b):
    return call_module("com_zz_lt", b, a)


register_module("com_zz_gt", com_zz_gt, Boolean, (Number, Number), "Returns if second integer is lesser than first")


# a <= b
def com_zz_le(a, b):
    return Boolean(call_module("com_zz_lt", a, b) or call_module("com_zz_eq", a, b))


register_module("com_zz_le", com_zz_le, Boolean, (Number, Number), "Returns if first integer is lesser or equal to second")


# a >= b
def com_zz_ge(a, b):
    return Boolean(call_module("com_zz_gt", a, b) or call_module("com_zz_eq", a, b))


register_module("com_zz_ge", com_zz_ge, Boolean, (Number, Number), "Returns if second integer is lesser or equal to first")

__all__ = []
