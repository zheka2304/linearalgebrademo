# Created by Dmitry Elizarov 14.03.2019
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


def led_p_q(a):
    if len(a) == 0:
        return Fraction(0)
    return a[len(a) - 1]


register_module("led_p_q",
                led_p_q, Fraction, (Polynomial, ),
                """
                Returns polynomial factor for greatest degree
                """)

__all__ = []

