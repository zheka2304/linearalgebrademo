# Created by Helen 23.03.19(C)
# Additional module for trigonometry functions
# import module registry
from .registry import *

# import structures like Number, Fraction, ...
from structures import *


##  FANCY
sin_table = [
    Fraction(numerator=0, denominator=5040, reduction=False),
    Fraction(numerator=1621632730750836298136410537, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=25334152315460079825224161, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=2223558519537250023255721, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=395604991121167024332967, denominator=5671229297435706421231920, reduction=False),
    Fraction(numerator=1619657502146026702918316785, denominator=18583484161837322801092755456, reduction=False),
    Fraction(numerator=34695487646055725281057, denominator=331923799758468414228480, reduction=False),
    Fraction(numerator=1617683717381314465794193849, denominator=13273917258455230572209111040, reduction=False),
    Fraction(numerator=49330164635260562250098, denominator=354451831089731651326995, reduction=False),
    Fraction(numerator=3039009502524845661097, denominator=19426724448598059909120, reduction=False),
    Fraction(numerator=25210847114258274925361065, denominator=145183470014354084383537152, reduction=False),
    Fraction(numerator=17729481204422959826908007867, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=539147220341873766391, denominator=2593154685613034486160, reduction=False),
    Fraction(numerator=20901873521824308350580525997, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=25087902283717023285253873, denominator=103702478581681488845383680, reduction=False),
    Fraction(numerator=2199250125372468403271665, denominator=8497249273816791404249088, reduction=False),
    Fraction(numerator=97700173555289647558228, denominator=354451831089731651326995, reduction=False),
    Fraction(numerator=27166426954569977823502300313, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=46899910085413077601, denominator=151771284754672343040, reduction=False),
    Fraction(numerator=30250955671225471144900924723, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=387934962780173528384335, denominator=1134245859487141284246384, reduction=False),
    Fraction(numerator=2175101459379942851522233, denominator=6069463767011993860177920, reduction=False),
    Fraction(numerator=271933447194201361224003491, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=36305731535333247119255405567, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=65920694576917563746, denominator=162072167850814655385, reduction=False),
    Fraction(numerator=7853720368273737586452088925, denominator=18583484161837322801092755456, reduction=False),
    Fraction(numerator=318221244736478711753421829, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=979949885791440879723, denominator=2158524938733117767680, reduction=False),
    Fraction(numerator=380354437740532083637879, denominator=810175613919386631604560, reduction=False),
    Fraction(numerator=45047262432604809121696122653, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=33192381992158500810985, denominator=66384759951693682845696, reduction=False),
    Fraction(numerator=47856012183630361106653711927, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=187830862642660290836072, denominator=354451831089731651326995, reduction=False),
    Fraction(numerator=23139669152373822728357051, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=405927842796476313142054481, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=1522721257948591865658278785, denominator=2654783451691046114441822208, reduction=False),
    Fraction(numerator=696944711646717127, denominator=1185713162145877680, reduction=False),
    Fraction(numerator=55919098842678172277432040853, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=446919329455117593425371099, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=26737459430545909511515309, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=45567444608367314668370, denominator=70890366217946330265399, reduction=False),
    Fraction(numerator=60959304594338980783720478657, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=31728619017539029662769, denominator=47417685679781202032640, reduction=False),
    Fraction(numerator=63369513702799267533202890907, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=3939565724305814688585797, denominator=5671229297435706421231920, reduction=False),
    Fraction(numerator=2747352691274560902385, denominator=3885344889719611981824, reduction=False),
    Fraction(numerator=522181000427391914939003399, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=67955461266301617769328778023, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=120443010398707643764, denominator=162072167850814655385, reduction=False),
    Fraction(numerator=10017944262149415328062604159, denominator=13273917258455230572209111040, reduction=False),
    Fraction(numerator=111216880731061380666910325, denominator=145183470014354084383537152, reduction=False),
    Fraction(numerator=33017976029471506717039001, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=4468983461344753162884787, denominator=5671229297435706421231920, reduction=False),
    Fraction(numerator=74207030161180903102865436677, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=13642812482064370131, denominator=16863476083852482560, reduction=False),
    Fraction(numerator=15222664762619747041140639635, denominator=18583484161837322801092755456, reduction=False),
    Fraction(numerator=41979016273888059596594, denominator=50635975869961664475285, reduction=False),
    Fraction(numerator=35631855573982564868543539, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=615610647508738549297527029, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=79645448810610034544223397643, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=449145446744313907615, denominator=518630937122606897232, reduction=False),
    Fraction(numerator=81266966065747820462416491517, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=640942981625417442296375551, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=2472744949491230407609, denominator=2775246349799722844160, reduction=False),
    Fraction(numerator=318576595677320045966032, denominator=354451831089731651326995, reduction=False),
    Fraction(numerator=16842199615075666021761789205, denominator=18583484161837322801092755456, reduction=False),
    Fraction(numerator=303224265441712563500387, denominator=331923799758468414228480, reduction=False),
    Fraction(numerator=85529906433891628079053836163, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=5258200369296565928172023, denominator=5671229297435706421231920, reduction=False),
    Fraction(numerator=39663713941577630559962879, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=19489349565286789952843065, denominator=20740495716336297769076736, reduction=False),
    Fraction(numerator=87853410709831124937213597647, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=70478443628292818, denominator=74107072634117355, reduction=False),
    Fraction(numerator=88855142099954071465705984417, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=697776861595882911007615021, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=8207452622777321819263325, denominator=8497249273816791404249088, reduction=False),
    Fraction(numerator=5502574176403089259373293, denominator=5671229297435706421231920, reduction=False),
    Fraction(numerator=12933193391993502875406871019, denominator=13273917258455230572209111040, reduction=False),
    Fraction(numerator=324656030867783658431557, denominator=331923799758468414228480, reduction=False),
    Fraction(numerator=91205735057619801791902615303, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=69809512382160294758740, denominator=70890366217946330265399, reduction=False),
    Fraction(numerator=2131818192080589446937, denominator=2158524938733117767680, reduction=False),
    Fraction(numerator=718803320557683386494308281, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=92217771851045137969449926387, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=368389979179928642503, denominator=370450669373290640880, reduction=False),
    Fraction(numerator=18511021257957577712122709345, denominator=18583484161837322801092755456, reduction=False),
    Fraction(numerator=724073261823573258231780739, denominator=725917350071770421917685760, reduction=False),
    Fraction(numerator=42423100433987074417981469, denominator=42486246369083957021245440, reduction=False),
    Fraction(numerator=354190436305429270719238, denominator=354451831089731651326995, reduction=False),
    Fraction(numerator=92890078868865038010375724433, denominator=92917420809186614005463777280, reduction=False),
    Fraction(numerator=30349494406877671465, denominator=30354256950934468608, reduction=False),
]

cos_table = [
    Fraction(numerator=1, denominator=1, reduction=False),
    Fraction(numerator=3262511203593848997523919, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=50953444357398892393319, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=4469871516555031603919, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=794692297451450810069, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=650118288456625454643931, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=69554464000209668519, denominator=69937589498202362880, reduction=False),
    Fraction(numerator=3238686203364457917325631, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=49305004867116816701, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=6064332867611040719, denominator=6139925552654254080, reduction=False),
    Fraction(numerator=10041986690730537192979, denominator=10196900548837904507904, reduction=False),
    Fraction(numerator=3203057514709264564670759, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=1068895079746176869, denominator=1092774835909411920, reduction=False),
    Fraction(numerator=3179377472771648429989271, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=49470044847021173558231, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=864697900596137086939, denominator=895201145576990244864, reduction=False),
    Fraction(numerator=47860790228998115189, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=3120430208207655721395311, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=91240877208927719, denominator=95936336760222720, reduction=False),
    Fraction(numerator=3085234800655294773777239, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=149718000649821338761, denominator=159326571075592257936, reduction=False),
    Fraction(numerator=4178711250421463956031, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=47272006558315305034559, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=3003614764591145868722591, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=62393715382789301, denominator=68298427244338245, reduction=False),
    Fraction(numerator=591457912308447017207731, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=45824564390335787831471, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=607857027166995839, denominator=682213950294917120, reduction=False),
    Fraction(numerator=703384982427038002181, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=2853890839383328089257399, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=12113543572198861651, denominator=13987517899640472576, reduction=False),
    Fraction(numerator=2796943240484325896311439, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=42223923119731392221, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=3753892807335052229159, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=42268047589025321135111, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=534579631573222307851531, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=1212719785386869, denominator=1499005261878480, reduction=False),
    Fraction(numerator=2605951654710767054337671, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=40176287555021625993839, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=3478504522603992901271, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=7628187910319674921, denominator=9957910692224516121, reduction=False),
    Fraction(numerator=2462617867941559999925279, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=51973611077162039831, denominator=69937589498202362880, reduction=False),
    Fraction(numerator=2386404872094697092992231, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=573047308742989040909, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=868312161499036891, denominator=1227985110530850816, reduction=False),
    Fraction(numerator=35416592377554373121591, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=2225349589808000863610351, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=45700156615193789, denominator=68298427244338245, reduction=False),
    Fraction(numerator=2140702826434059026124719, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=6554356406480853884779, denominator=10196900548837904507904, reduction=False),
    Fraction(numerator=2816798047672207596911, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=490447098772591893821, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=1963684162450404554928551, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=6265387883850839, denominator=10659592973358080, reduction=False),
    Fraction(numerator=374305325909347316262931, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=27840944022763033949, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=2437701776300889316439, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=27016287903104848635599, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=1680472086945034386791879, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=109269722331797209, denominator=218554967181882384, reduction=False),
    Fraction(numerator=1581805580179960867539959, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=23933423475510483200039, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=2787146313921383231, denominator=6139925552654254080, reduction=False),
    Fraction(numerator=21823347711935324669, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=275757533315673370270531, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=28440875358468351359, denominator=69937589498202362880, reduction=False),
    Fraction(numerator=1274679869733987477758711, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=298347277132599887861, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=1603573281490922527391, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=3486309667975600096579, denominator=10196900548837904507904, reduction=False),
    Fraction(numerator=1061888843780282345860799, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=28936923160301, denominator=93687828867405, reduction=False),
    Fraction(numerator=953458984800044026404191, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=14043617151393144115871, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=231507220733748546739, denominator=895201145576990244864, reduction=False),
    Fraction(numerator=192537146118166395389, denominator=796632855377961289680, reduction=False),
    Fraction(numerator=733172733871526333434391, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=14520787240513055471, denominator=69937589498202362880, reduction=False),
    Fraction(numerator=621575806512548121793199, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=1725680347505806321, denominator=9957910692224516121, reduction=False),
    Fraction(numerator=106457638563819431, denominator=682213950294917120, reduction=False),
    Fraction(numerator=7073907918912497924279, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=396126855625440938743511, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=113661084019487381, denominator=1092774835909411920, reduction=False),
    Fraction(numerator=56507281343119154689531, denominator=652601635125625888505856, reduction=False),
    Fraction(numerator=3524716407726603608831, denominator=50984502744189522539520, reduction=False),
    Fraction(numerator=231197239032344854199, denominator=4476005727884951224320, reduction=False),
    Fraction(numerator=1700370158373883661, denominator=49789553461122580605, reduction=False),
    Fraction(numerator=54276058643966978607359, denominator=3263008175628129442529280, reduction=False),
    Fraction(numerator=-17166022742381, denominator=19187267352044544, reduction=False),
]


## FAST

sinf_table = [
    Fraction(numerator=0, denominator=120, reduction=False),
    Fraction(numerator=207928519883051, denominator=11909238516000000, reduction=False),
    Fraction(numerator=12993551616551, denominator=372163703625000, reduction=False),
    Fraction(numerator=2565975419051, denominator=49009212000000, reduction=False),
    Fraction(numerator=3246407480954, denominator=46520462953125, reduction=False),
    Fraction(numerator=332280078251, denominator=3810956325120, reduction=False),
    Fraction(numerator=160153500551, denominator=1531537875000, reduction=False),
    Fraction(numerator=86389739051, denominator=708588000000, reduction=False),
    Fraction(numerator=6476986071028, denominator=46520462953125, reduction=False),
    Fraction(numerator=31562923051, denominator=201684000000, reduction=False),
    Fraction(numerator=20688414791, denominator=119092385160, reduction=False),
    Fraction(numerator=2273293223406601, denominator=11909238516000000, reduction=False),
    Fraction(numerator=39818864954, denominator=191442234375, reduction=False),
    Fraction(numerator=2680055549542943, denominator=11909238516000000, reduction=False),
    Fraction(numerator=5359080551, denominator=22143375000, reduction=False),
    Fraction(numerator=4060642091, denominator=15682947840, reduction=False),
    Fraction(numerator=12827804841896, denominator=46520462953125, reduction=False),
    Fraction(numerator=3483284924375707, denominator=11909238516000000, reduction=False),
    Fraction(numerator=1948376551, denominator=6302625000, reduction=False),
    Fraction(numerator=3878772783418049, denominator=11909238516000000, reduction=False),
    Fraction(numerator=5093466554, denominator=14886548145, reduction=False),
    Fraction(numerator=1045403051, denominator=2916000000, reduction=False),
    Fraction(numerator=139468395695101, denominator=372163703625000, reduction=False),
    Fraction(numerator=4655085368250493, denominator=11909238516000000, reduction=False),
    Fraction(numerator=77896143028, denominator=191442234375, reduction=False),
    Fraction(numerator=322237715195, denominator=762191265024, reduction=False),
    Fraction(numerator=163207215042443, denominator=372163703625000, reduction=False),
    Fraction(numerator=30532303131, denominator=67228000000, reduction=False),
    Fraction(numerator=1299944954, denominator=2767921875, reduction=False),
    Fraction(numerator=5775855368915599, denominator=11909238516000000, reduction=False),
    Fraction(numerator=245136551, denominator=490092120, reduction=False),
    Fraction(numerator=6135966239499701, denominator=11909238516000000, reduction=False),
    Fraction(numerator=24661113742672, denominator=46520462953125, reduction=False),
    Fraction(numerator=26702061822601, denominator=49009212000000, reduction=False),
    Fraction(numerator=208186899061207, denominator=372163703625000, reduction=False),
    Fraction(numerator=130104491, denominator=226748160, reduction=False),
    Fraction(numerator=463240954, denominator=787828125, reduction=False),
    Fraction(numerator=7169740908884807, denominator=11909238516000000, reduction=False),
    Fraction(numerator=229209288494549, denominator=372163703625000, reduction=False),
    Fraction(numerator=30853586134943, denominator=49009212000000, reduction=False),
    Fraction(numerator=9572330548, denominator=14886548145, reduction=False),
    Fraction(numerator=7815977940989251, denominator=11909238516000000, reduction=False),
    Fraction(numerator=60996551, denominator=91125000, reduction=False),
    Fraction(numerator=8125027061007593, denominator=11909238516000000, reduction=False),
    Fraction(numerator=32327617162654, denominator=46520462953125, reduction=False),
    Fraction(numerator=45652651, denominator=64538880, reduction=False),
    Fraction(numerator=267811471744993, denominator=372163703625000, reduction=False),
    Fraction(numerator=8713133624728357, denominator=11909238516000000, reduction=False),
    Fraction(numerator=142323417896, denominator=191442234375, reduction=False),
    Fraction(numerator=534984838157, denominator=708588000000, reduction=False),
    Fraction(numerator=18253195895, denominator=23818477032, reduction=False),
    Fraction(numerator=38102628743707, denominator=49009212000000, reduction=False),
    Fraction(numerator=36673726325522, denominator=46520462953125, reduction=False),
    Fraction(numerator=9515165649737143, denominator=11909238516000000, reduction=False),
    Fraction(numerator=1700384631, denominator=2100875000, reduction=False),
    Fraction(numerator=3123159275401, denominator=3810956325120, reduction=False),
    Fraction(numerator=2295783028, denominator=2767921875, reduction=False),
    Fraction(numerator=41122544842049, denominator=49009212000000, reduction=False),
    Fraction(numerator=315772572247099, denominator=372163703625000, reduction=False),
    Fraction(numerator=10213617993916249, denominator=11909238516000000, reduction=False),
    Fraction(numerator=53083514, denominator=61261515, reduction=False),
    Fraction(numerator=10422144514154351, denominator=11909238516000000, reduction=False),
    Fraction(numerator=328803692198201, denominator=372163703625000, reduction=False),
    Fraction(numerator=10699051, denominator=12000000, reduction=False),
    Fraction(numerator=41841218681504, denominator=46520462953125, reduction=False),
    Fraction(numerator=3456435874463, denominator=3810956325120, reduction=False),
    Fraction(numerator=1400223299101, denominator=1531537875000, reduction=False),
    Fraction(numerator=10971652847293457, denominator=11909238516000000, reduction=False),
    Fraction(numerator=43171448711578, denominator=46520462953125, reduction=False),
    Fraction(numerator=45797658762493, denominator=49009212000000, reduction=False),
    Fraction(numerator=6665351, denominator=7085880, reduction=False),
    Fraction(numerator=11272747193949901, denominator=11909238516000000, reduction=False),
    Fraction(numerator=750151028, denominator=787828125, reduction=False),
    Fraction(numerator=11403272917598243, denominator=11909238516000000, reduction=False),
    Fraction(numerator=358233758860307, denominator=372163703625000, reduction=False),
    Fraction(numerator=3034153595, denominator=3136589568, reduction=False),
    Fraction(numerator=45209875398446, denominator=46520462953125, reduction=False),
    Fraction(numerator=691601742601, denominator=708588000000, reduction=False),
    Fraction(numerator=1500818190443, denominator=1531537875000, reduction=False),
    Fraction(numerator=11713558033273349, denominator=11909238516000000, reduction=False),
    Fraction(numerator=14691667496, denominator=14886548145, reduction=False),
    Fraction(numerator=66553132833, denominator=67228000000, reduction=False),
    Fraction(numerator=369456835542751, denominator=372163703625000, reduction=False),
    Fraction(numerator=11852127537009793, denominator=11909238516000000, reduction=False),
    Fraction(numerator=11360954, denominator=11390625, reduction=False),
    Fraction(numerator=3808284770587, denominator=3810956325120, reduction=False),
    Fraction(numerator=372504783392093, denominator=372163703625000, reduction=False),
    Fraction(numerator=49119396419599, denominator=49009212000000, reduction=False),
    Fraction(numerator=46673748450428, denominator=46520462953125, reduction=False),
    Fraction(numerator=11957562480938899, denominator=11909238516000000, reduction=False),
    Fraction(numerator=2025991, denominator=2016840, reduction=False),
]

cosf_table = [
    Fraction(numerator=1, denominator=1, reduction=False),
    Fraction(numerator=3780134355841, denominator=3780710640000, reduction=False),
    Fraction(numerator=236150354941, denominator=236294415000, reduction=False),
    Fraction(numerator=46611421441, denominator=46675440000, reduction=False),
    Fraction(numerator=29464793807, denominator=29536801875, reduction=False),
    Fraction(numerator=6026099713, denominator=6049137024, reduction=False),
    Fraction(numerator=2901221341, denominator=2917215000, reduction=False),
    Fraction(numerator=1562893441, denominator=1574640000, reduction=False),
    Fraction(numerator=29249120987, denominator=29536801875, reduction=False),
    Fraction(numerator=569139841, denominator=576240000, reduction=False),
    Fraction(numerator=372322717, denominator=378071064, reduction=False),
    Fraction(numerator=3711192844081, denominator=3780710640000, reduction=False),
    Fraction(numerator=356677007, denominator=364651875, reduction=False),
    Fraction(numerator=3683734304401, denominator=3780710640000, reduction=False),
    Fraction(numerator=95489341, denominator=98415000, reduction=False),
    Fraction(numerator=72134017, denominator=74680704, reduction=False),
    Fraction(numerator=28391700467, denominator=29536801875, reduction=False),
    Fraction(numerator=3615383117761, denominator=3780710640000, reduction=False),
    Fraction(numerator=34250941, denominator=36015000, reduction=False),
    Fraction(numerator=3574574802961, denominator=3780710640000, reduction=False),
    Fraction(numerator=44406671, denominator=47258883, reduction=False),
    Fraction(numerator=18147841, denominator=19440000, reduction=False),
    Fraction(numerator=219075735181, denominator=236294415000, reduction=False),
    Fraction(numerator=3479945726881, denominator=3780710640000, reduction=False),
    Fraction(numerator=333103787, denominator=364651875, reduction=False),
    Fraction(numerator=5481988849, denominator=6049137024, reduction=False),
    Fraction(numerator=212363952301, denominator=236294415000, reduction=False),
    Fraction(numerator=171130907, denominator=192080000, reduction=False),
    Fraction(numerator=10861007, denominator=12301875, reduction=False),
    Fraction(numerator=3306398650321, denominator=3780710640000, reduction=False),
    Fraction(numerator=4041853, denominator=4667544, reduction=False),
    Fraction(numerator=3240408764161, denominator=3780710640000, reduction=False),
    Fraction(numerator=25046350547, denominator=29536801875, reduction=False),
    Fraction(numerator=39141781681, denominator=46675440000, reduction=False),
    Fraction(numerator=195879657661, denominator=236294415000, reduction=False),
    Fraction(numerator=2063617, denominator=2519424, reduction=False),
    Fraction(numerator=3641807, denominator=4501875, reduction=False),
    Fraction(numerator=3019197174001, denominator=3780710640000, reduction=False),
    Fraction(numerator=186191478061, denominator=236294415000, reduction=False),
    Fraction(numerator=36271990801, denominator=46675440000, reduction=False),
    Fraction(numerator=36201419, denominator=47258883, reduction=False),
    Fraction(numerator=2853324324001, denominator=3780710640000, reduction=False),
    Fraction(numerator=902941, denominator=1215000, reduction=False),
    Fraction(numerator=2765188824241, denominator=3780710640000, reduction=False),
    Fraction(numerator=21249000287, denominator=29536801875, reduction=False),
    Fraction(numerator=652033, denominator=921984, reduction=False),
    Fraction(numerator=164176050781, denominator=236294415000, reduction=False),
    Fraction(numerator=2579110000321, denominator=3780710640000, reduction=False),
    Fraction(numerator=244081667, denominator=364651875, reduction=False),
    Fraction(numerator=1033494241, denominator=1574640000, reduction=False),
    Fraction(numerator=243146989, denominator=378071064, reduction=False),
    Fraction(numerator=29392676161, denominator=46675440000, reduction=False),
    Fraction(numerator=18198812927, denominator=29536801875, reduction=False),
    Fraction(numerator=2277411843121, denominator=3780710640000, reduction=False),
    Fraction(numerator=7064207, denominator=12005000, reduction=False),
    Fraction(numerator=3474209713, denominator=6049137024, reduction=False),
    Fraction(numerator=6889787, denominator=12301875, reduction=False),
    Fraction(numerator=25467484561, denominator=46675440000, reduction=False),
    Fraction(numerator=125482893421, denominator=236294415000, reduction=False),
    Fraction(numerator=1952024799601, denominator=3780710640000, reduction=False),
    Fraction(numerator=292559, denominator=583443, reduction=False),
    Fraction(numerator=1839019763281, denominator=3780710640000, reduction=False),
    Fraction(numerator=111359899261, denominator=236294415000, reduction=False),
    Fraction(numerator=109441, denominator=240000, reduction=False),
    Fraction(numerator=13014265427, denominator=29536801875, reduction=False),
    Fraction(numerator=2571518737, denominator=6049137024, reduction=False),
    Fraction(numerator=1194569581, denominator=2917215000, reduction=False),
    Fraction(numerator=1488737889361, denominator=3780710640000, reduction=False),
    Fraction(numerator=11163669647, denominator=29536801875, reduction=False),
    Fraction(numerator=16899029281, denominator=46675440000, reduction=False),
    Fraction(numerator=54493, denominator=157464, reduction=False),
    Fraction(numerator=1247640810721, denominator=3780710640000, reduction=False),
    Fraction(numerator=1412987, denominator=4501875, reduction=False),
    Fraction(numerator=1125392981281, denominator=3780710640000, reduction=False),
    Fraction(numerator=66495741901, denominator=236294415000, reduction=False),
    Fraction(numerator=19798129, denominator=74680704, reduction=False),
    Fraction(numerator=7347378047, denominator=29536801875, reduction=False),
    Fraction(numerator=365893681, denominator=1574640000, reduction=False),
    Fraction(numerator=629973901, denominator=2917215000, reduction=False),
    Fraction(numerator=754297965121, denominator=3780710640000, reduction=False),
    Fraction(numerator=8651171, denominator=47258883, reduction=False),
    Fraction(numerator=32000267, denominator=192080000, reduction=False),
    Fraction(numerator=35476811101, denominator=236294415000, reduction=False),
    Fraction(numerator=505425484561, denominator=3780710640000, reduction=False),
    Fraction(numerator=17807, denominator=151875, reduction=False),
    Fraction(numerator=609953857, denominator=6049137024, reduction=False),
    Fraction(numerator=19954960141, denominator=236294415000, reduction=False),
    Fraction(numerator=3178819921, denominator=46675440000, reduction=False),
    Fraction(numerator=1530208667, denominator=29536801875, reduction=False),
    Fraction(numerator=134456995681, denominator=3780710640000, reduction=False),
    Fraction(numerator=1117, denominator=57624, reduction=False),
]

PI2 = Fraction(numerator=710, denominator=113, reduction=False)
PI360 = Fraction(numerator=71, denominator=4068, reduction=False)


def to_circle(f):
    d = f / PI360
    i = call_module("int", d) % Number(360)
    return int(i), f - d * i


def t_sin(f, table=None):
    if table is None:
        table = sin_table
    index, f = to_circle(f)
    q = index // 90
    if q % 2 == 0:
        r = table[index % 90]
    else:
        r = table[89 - index % 90]
    if q > 1:
        return r.opposite()
    else:
        return r


def t_cos(f, table=None):
    if table is None:
        table = cos_table
    index, f = to_circle(f)
    q = index // 90
    if q % 2 == 0:
        r = table[index % 90]
    else:
        r = table[89 - index % 90]
    if 0 < q < 3:
        return r.opposite()
    else:
        return r


def t_sinf(f):
    return t_sin(f, table=sinf_table)


def t_cosf(f):
    return t_cos(f, table=cosf_table)


register_module("sin", t_sin, Fraction, (Fraction, ), "Returns sin of fraction in radians")
register_module("cos", t_cos, Fraction, (Fraction, ), "Returns cos of fraction in radians")
register_module("sinf", t_sinf, Fraction, (Fraction, ), "Returns sin of fraction in radians but faster")
register_module("cosf", t_cosf, Fraction, (Fraction, ), "Returns cos of fraction in radians but faster")


__all__ = []


